#pragma once
#define _mm_stream_load_ps(mem_addr) _mm_castsi128_ps(_mm_stream_load_si128((const __m128i*)mem_addr))

#define _mm256_stream_load_ps(mem_addr) _mm256_castsi256_ps(_mm256_stream_load_si256((const __m256i*)mem_addr))

// Prefetches mem_addr to L1 cache
#define _mm_prefetch_L1(mem_addr) _mm_prefetch((const char *)mem_addr, _MM_HINT_T0)

// Prefetches mem_addr to L2 cache
#define _mm_prefetch_L2(mem_addr) _mm_prefetch((const char *)mem_addr, _MM_HINT_T1)

// Prefetches mem_addr to L3 cache
#define _mm_prefetch_L3(mem_addr) _mm_prefetch((const char *)mem_addr, _MM_HINT_T2)